//
//  GunSpec.swift
//  BDDtestDemo
//
//  Created by Aya on 3/2/20.
//  Copyright © 2020 Aya. All rights reserved.
//

import Foundation
import Quick
@testable import BDDtestDemo

class GunSpec : QuickSpec {
  override func spec() {
    super.spec()

    describe("shoot") {
      context("has bullets") {
        context("is unlocked") {
          it("can shoot") {
            let gun = Gun(bullets: 1)
            gun.isLocked = false
            gun.shoot()
            XCTAssertTrue(gun.bullets == 0, "expect Gun to be out of bullet")
          }
        }

        context("is locked") {
          it("cannot shoot") {
            let gun = Gun(bullets: 1)
            gun.isLocked = true
            gun.shoot()
            XCTAssertTrue(gun.bullets == 1, "expect Gun to not shoot anything")
          }
        }
      }

      context("has no bullet") {
        it("cannot shoot") {
            let gun = Gun(bullets: 0)
            gun.shoot()
            XCTAssertTrue(gun.bullets == 0, "expect Gun to not shoot anything")
        }
      }
    }

    describe("lock") {
      it("locks the Gun") {
        let gun = Gun(bullets: 2)
        gun.lock()
        XCTAssertTrue(gun.isLocked, "expect Gun to be locked")
      }
    }

    describe("unlock") {
      it("unlocks the Gun") {
        let gun = Gun(bullets: 2)
        gun.unlock()
        // notice that we use assert False here
        XCTAssertFalse(gun.isLocked, "expect Gun to be unlocked")
      }
    }

  }
}
