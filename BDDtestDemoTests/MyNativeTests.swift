//
//  MyNativeTests.swift
//  BDDtestDemoTests
//
//  Created by Aya on 3/2/20.
//  Copyright © 2020 Aya. All rights reserved.
//

import XCTest
@testable import BDDtestDemo

class MyNativeTests: XCTestCase {

  func testGunCanShootIfItHasBullets() {
    // 1. Setup: create a gun with 1 bullet
    let gun = Gun(bullets: 1)
    // 2. Execution: shoot
    gun.shoot()
    // 3. Expectation: expect Gun to be out of bullet
    XCTAssertTrue(gun.bullets == 0, "expect Gun to be out of bullet")
  }
    
    func testGunCannotShootIfItHasNoBullet() {
      // 1. Setup: create a gun with no bullet
      let gun = Gun(bullets: 0)
      // 2. Execution: shoot
      gun.shoot()
      // 3. Expectation: expect Gun to not shoot anything
      XCTAssertTrue(gun.bullets == 0, "expect the number of bullets to remain the same")
    }
}
