//
//  Gun.swift
//  BDDtestDemo
//
//  Created by Aya on 3/2/20.
//  Copyright © 2020 Aya. All rights reserved.
//

import Foundation
class Gun : NSObject {

  var bullets = 0
  var isLocked = true

  init(bullets: Int) {
    self.bullets = bullets
  }

  func shoot() {
    if self.bullets > 0 {
      if !isLocked {
        self.bullets -= 1
      }
    }
  }

  func lock() {
    isLocked = true
  }

  func unlock() {
    isLocked = false
  }
}
